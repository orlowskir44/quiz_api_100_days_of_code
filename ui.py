from tkinter import *

import data
from quiz_brain import QuizBrain
from data import new_data

THEME_COLOR = "#375362"
FONT = ('Arial', 20, 'italic')
WHITE = 'white'
RED = 'red'
GREEN = 'green'


class QuizInterface:

    def __init__(self, question: QuizBrain):
        new_data()
        self.question = question
        self.window = Tk()
        self.window.title("Quizz Game")
        self.window.config(padx=20, pady=20, bg=THEME_COLOR, highlightthickness=0)

        # BG
        self.canvas = Canvas(width=300, height=250, bg=WHITE)
        self.canvas.grid(column=0, row=1, columnspan=2, pady=20)

        # Text
        self.question_text = self.canvas.create_text(150, 125, width=280, text='Questions', fill='black', font=FONT)
        # Score:
        self.score = Label(text='Score', pady=5, highlightthickness=0, bg=THEME_COLOR)
        self.score.grid(column=1, row=0)

        # Buttons:
        self.false_i = PhotoImage(file="./images/false.png")
        self.false_b = Button(image=self.false_i, command=self.false_ans, highlightthickness=0, borderwidth=0)
        self.false_b.grid(column=0, row=2)

        self.true_i = PhotoImage(file="./images/true.png")
        self.true_b = Button(image=self.true_i, command=self.true_ans, highlightthickness=0, borderwidth=0)
        self.true_b.grid(column=1, row=2)

        self.reset_b = \
            Button(text="reset", command=self.reset_questions,
                   background=THEME_COLOR, highlightthickness=0, bd=0, state='normal')
        self.reset_b.grid(column=0, row=0)

        self.get_next_question()

        self.window.mainloop()

    def true_ans(self):
        self.give_feedback(self.question.check_answer('True'))

    def false_ans(self):
        self.give_feedback(self.question.check_answer('False'))

    def reset_questions(self):
        self.reset_values()
        self.question_score()

        data.reset()

        q = self.question.next_question()
        self.canvas.itemconfig(self.question_text, text=q)

    def give_feedback(self, answer: bool):
        if answer:
            self.canvas.config(bg=GREEN)
        else:
            self.canvas.config(bg=RED)
        self.window.after(1000, self.get_next_question)

    def get_next_question(self):
        self.canvas.config(bg=WHITE)
        self.question_score()

        if self.question.still_has_questions():
            question = self.question.next_question()
            self.canvas.itemconfig(self.question_text, text=question)
        else:
            self.canvas.itemconfig(self.question_text,
                                   text=f'Well played! The quiz is over! Score:{self.question.score}')
            self.true_b.config(state='disabled')
            self.false_b.config(state='disabled')
            self.reset_b.config(state='normal')

    def question_score(self):
        self.score.config(text=f'Score: {self.question.score}/{len(self.question.question_list)}')

    def reset_values(self):
        self.true_b.config(state='normal')
        self.false_b.config(state='normal')
        # self.reset_b.config(state='disabled')

        self.question.question_number = 0
        self.question.score = 0
