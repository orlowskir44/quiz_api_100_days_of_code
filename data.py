import requests as r
from quiz_brain import QuizBrain
from question_model import Question

par = {
    "amount": 10,
    "type": "boolean"
}
question_bank = []


def new_data():
    raw_data = r.get(url="https://opentdb.com/api.php", params=par)
    raw_data.raise_for_status()
    question_data = raw_data.json()['results']
    # ANSWER CHECK:
    # for x in question_data:
    #     print(x['correct_answer'], end=' ')
    # print('\n')

    for question in question_data:
        question_text = question["question"]
        question_answer = question["correct_answer"]
        new_question = Question(question_text, question_answer)
        question_bank.append(new_question)


def reset():
    question_bank.clear()
    new_data()


quiz = QuizBrain(question_bank)
